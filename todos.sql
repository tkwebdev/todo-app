DROP TABLE IF EXISTS todos;

CREATE TABLE todos (
    todo_id integer PRIMARY KEY,
    name text
);

INSERT INTO todos VALUES (1, 'shopping');
INSERT INTO todos VALUES (2, 'gym');
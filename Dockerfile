FROM payara/micro
COPY target/ROOT.war $DEPLOY_DIR
CMD ["--nocluster", "--deploymentDir", "/opt/payara/deployments"]
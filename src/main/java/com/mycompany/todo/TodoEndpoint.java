package com.mycompany.todo;

import com.mycompany.todo.model.Todo;

import java.util.Collection;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Path("/todo")
public class TodoEndpoint {

  @PersistenceContext(unitName = "com.mycompany.todo.jpa")
  private EntityManager entityManager;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Collection<Todo> getAllTodos() {
    return entityManager.createNamedQuery("Todo.findAll", Todo.class)
                .getResultList();
  }
}
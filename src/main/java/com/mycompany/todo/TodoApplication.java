package com.mycompany.todo;

import javax.ws.rs.core.Application;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("/todo-service")
public class TodoApplication extends Application {}
package com.mycompany.todo.model;

import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@Table(name = "todos")
@NamedQueries({
  @NamedQuery(name = "Todo.findAll", query = "SELECT t from Todo t")
})
public class Todo {

  @Id
  @Column(name = "todo_id", unique = true)
  private int id;

  @Column(name = "name")
  private String name;

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
      this.id = id;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String name) {
      this.name = name;
  }
}